Custom files for mpv.

These files are modified to taste.
Read first before deploying!

To deploy, open a terminal in the same directory as this file and then:

~~~shell
./deploy.sh
~~~

Files will be sent to their respective destinations as specified 
by the Arch Wiki.

If you need them elsewhere, modify deploy.sh to your needs.
